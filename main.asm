%include "words.inc"

global _start
extern read_word
extern find_word
extern string_length
extern print_string
extern print_newline
extern exit
extern print_error

%define BUFFER_POINTER 255

section .data

err: db 'This word is not in the dictionary', 10, 0

section .text
_start:
    sub rsp, BUFFER_POINTER
    mov rdi, rsp
    mov rsi, BUFFER_POINTER
    call read_word

    mov rsi, recent
    mov rdi, rax
    call find_word

    add rsp, BUFFER_POINTER
    
    test rax, rax
    jnz .success
    mov rdi, err
    call print_error
    call exit

.success:
    mov rdi, rax
    call string_length

    add rdi, rax
    inc rdi
    call print_string
    call print_newline

    call exit
    
