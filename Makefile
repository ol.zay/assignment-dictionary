NASM = nasm -f elf64
LIB = lib.asm
DICT = dict.asm
MAIN = main.asm
NAME = second_lab

all: build

objects: $(LIB) $(DICT) $(MAIN)
	$(NASM) $(LIB) -o $(LIB:.asm=.o)
	$(NASM) $(DICT) -o $(DICT:.asm=.o)
	$(NASM) $(MAIN) -o $(MAIN:.asm=.o)

build: objects $(LIB) $(DICT) $(MAIN)
	ld -o $(NAME) $(LIB:.asm=.o) $(DICT:.asm=.o) $(MAIN:.asm=.o)
	
