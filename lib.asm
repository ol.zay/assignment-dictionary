section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy
global print_error
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    mov rax, 0
.loop:
    cmp byte[rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    call string_length
    mov rsi, rdi
    mov rdi, 1	; descriptor stdout
    mov rdx, rax	; string length in bytes
    mov rax, 1	; 'write' syscall number
    syscall
    ret

; Принимает код символа и выводит его в stdout
print_char:
    push rdi	
    mov rdi, rsp	; for correct print_string work char adress move to rdi
    call print_string
    pop rdi	; return stack to starting position
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA	; for coorect print_char work
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov r10, 10
    mov rax, rdi
    mov rdi, rsp
    dec rdi
    push 0
    sub rsp, 16
.loop:
    xor rdx, rdx
    div r10
    add rdx, '0'
    dec rdi
    mov [rdi], dl
    cmp rax, 0
    jne .loop
.end:
    call print_string
    add rsp, 24
    ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    cmp rdi, 0
    jl .negative
.positive:
    jmp print_uint
.negative:
    push rdi
    mov rdi, '-'
    call print_char
    pop rdi
    neg rdi
    jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
    xor rcx, rcx
    xor r9, r9
    xor r10, r10
.loop:
    mov r9b, byte [rdi + rcx]
    mov r10b, byte [rsi + rcx]
    cmp r9b, r10b
    jne .ne
    cmp r9b, 0
    je .e
    inc rcx
    jmp .loop
.e:                 ; if equals
    mov rax, 1
    ret
.ne:                ; if not equals
    mov rax, 0
    ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    push 0
    mov rdi, 0
    mov rax, 0
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    ret 

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    xor r9, r9
    mov r10, rdi
.loop:
    push rsi
    push rdi
    call read_char
    pop rdi
    pop rsi
    test r9, r9
    je .space_check
.buf_check:
    cmp r9, rsi
    jae .error
    cmp rax, 0x20   ; compare with space symbol
    je .addnull
    cmp rax, 0x9    ; compare with tab symbol
    je .addnull
    cmp rax, 0xA    ; compare with line feed symbol
    je .addnull
    mov [rdi + r9], rax
    test rax, rax
    je .end
    inc r9
    jmp .loop
.space_check:
    cmp rax, 0x20   ; compare with space symbol
    je .loop
    cmp rax, 0x9    ; compare with tab symbol
    je .loop
    cmp rax, 0xA    ; compare with line feed symbol
    je .loop
    jmp .buf_check
.addnull:
    cmp r9, rsi
    jae .error
    xor rax, rax
    mov [rdi + r9], rax
    jmp .end
.error:
    xor rax, rax
    ret
.end:
    mov rax, r10
    mov rdx, r9
    ret
    
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor r9, r9
    xor rdx, rdx
    xor rax, rax
.loop:
    cmp byte [rdi + rdx], '0'
    jl .end
    cmp byte [rdi + rdx], '9'
    jg .end
    mov r9b, byte [rdi + rdx]
    sub r9b, '0'
    imul rax, 10
    add rax, r9
    inc rdx
    jmp .loop
.end:
    ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    cmp byte [rdi], '-'
    jne .positive
    inc rdi
    call parse_uint
    inc rdx
    neg rax
    ret
.positive:
    jmp parse_uint

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    call string_length
    cmp rax, rdx
    jae .over
    xor rax, rax
.loop:
    mov rcx, [rdi + rax]
    mov [rsi + rax], rcx
    cmp byte [rdi + rax], 0
    je .end
    inc rax
    jmp .loop
.end:
    inc rax
    ret
.over:
    mov rax, 0
    ret

print_error:
    call string_length
    mov rsi, rdi
    mov rdx, rax
    
    mov rax, 1
    mov rdi, 2
    syscall
    ret
